package main

import (
	"database/sql"
	"github.com/gin-gonic/gin"
	"strconv"
	"log"
	"os"
	_ "github.com/lib/pq"
)

var (
	PORT = os.Getenv("PORT")
	//DATABASE_URL = os.Getenv("DATABASE_URL")
)

//Delcare User Structure
type User struct {
	Id int64 `db:"id" json:"id"`
	Name string `db:"name" json:"name"`
	Email string `db:"email" json:"email"`
}

//Test DB connection, setup routes for CRUD API 
func main() {
	
	//DB Connection
	log.Println("Opening connection to database ... ")
	db, err := sql.Open("postgres", "postgres://postgres:postgres@localhost/postgres?sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	// Ping database connection to check connection are OK
	log.Println("Ping database connection ... ")
	err = db.Ping()
	if err != nil {
		log.Println("Ping database connection: failure :(")
		log.Fatal(err)
	}
	log.Println("Ping database connection: success!")

	// Run crud routes
	r := gin.Default()

	v1 := r.Group("api/v1")
	{
		v1.GET("/users", GetUsers)
		v1.GET("/users/:id", GetUser)

	}
	r.Run(":8080")
}

//Get All Users
func GetUsers(c *gin.Context) {
	type Users []User

	//Fill Users Temp
	var users = Users{
		User{Id: 1, Name: "Matt", Email: "matt@matthewgerard.com"},
		User{Id: 2, Name: "Tester", Email: "fake@fake.com"},
	}

	c.JSON(200, users)
	// curl -i http://localhost:8080/api/v1/users
}

//Get User by ID
func GetUser(c *gin.Context) {
	id := c.Params.ByName("id")
	user_id, _ := strconv.ParseInt(id,0,64)

	if user_id == 1 {
		content := gin.H{"id": user_id, "name": "Matt", "email": "matt@matthewgerard.com"}
		c.JSON(200, content)
	} else if user_id == 2 {
		content := gin.H{"id": user_id, "name": "Tester", "email": "fake@fake.com"}
		c.JSON(200, content)
	} else {
		content := gin.H{"error": "user id:" + id + " not found"}
		c.JSON(404, content)
	}
	// curl -i http://localhost:8080/api/v1/users/1
}

